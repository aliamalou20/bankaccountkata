package com.bank.account.kata;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class AmountTest {
	
	@Test
	public void should_add_amount_to_an_other() throws Exception {
		Amount amount = new Amount(100);
		amount = amount.add(new Amount(200));
		Amount expectedAmount = new Amount(300);
		Assertions.assertThat(amount.getValue()).isEqualTo(expectedAmount.getValue());
	}
	
	@Test
	public void should_substract_amount_to_an_other() throws Exception {
		Amount amount = new Amount(100);
		amount = amount.substruct(new Amount(50));
		Amount expectedAmount = new Amount(50);
		Assertions.assertThat(amount.getValue()).isEqualTo(expectedAmount.getValue());
	}
	
	@Test
	public void should_return_true_when_result_is_negative() throws Exception {
		Amount amount = new Amount(50);
		Assertions.assertThat(amount.isNegative(new Amount(100))).isTrue();
	}
	
	@Test
	public void should_return_false_when_result_is_positive() throws Exception {
		Amount amount = new Amount(100);
		Assertions.assertThat(amount.isNegative(new Amount(50))).isFalse();
	}
	

}
