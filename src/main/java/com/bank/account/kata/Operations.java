package com.bank.account.kata;

public interface Operations {
	
	void register (Amount amount, Operation operation);
	
	Statement generateStatement();
}
