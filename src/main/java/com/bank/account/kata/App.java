package com.bank.account.kata;

import java.time.LocalDate;


/**
 * ali_amalou
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	Amount amount = new Amount(2000);
    	
    	Operations operations = new HistoricOperations(LocalDate.of(2019, 3, 30));
    	
    	Account account = new Account(operations);
    	
    	
    	account.deposit(amount);
    	account.deposit(new Amount(500));
    	account.withdraw(amount);
    	
        account.printStatement();    
    }
}
