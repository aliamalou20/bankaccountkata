package com.bank.account.kata;

public class Amount {

	private int value;
	
	
	public Amount(int value) {
		this.value = value;
	}

	public Amount add(Amount amount) {
		return new Amount(this.value + amount.value);
	}
	
	public Amount substruct(Amount amount) {
		return new Amount(this.value - amount.value);
	}
	
	public int getValue() {
		return value;
	}
	
	public boolean isNegative(Amount amount) {
		return value - amount.getValue() < 0;
	}


	@Override
	public String toString() {
		return "Amount [value=" + value + "]";
	}

	
	

}
