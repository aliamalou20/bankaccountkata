package com.bank.account.kata;

import java.time.LocalDate;

public class StatementLine {
	
	private LocalDate date;
	private Amount amount;
	private final int balance;
	private Operation operation;
	
	
	public StatementLine(LocalDate date, Amount amount, int balance, Operation operation) {
		this.date = date;
		this.amount = amount;
		this.balance = balance;
		this.operation = operation;
	}
	
	
	public Amount getAmount() {
		return amount;
	}

	public int getBalance() {
		return balance;
	}


	public String printStatementLine() {
		return this.toString();
	}
	
	@Override
	public String toString() {
		return date + "   |      " + operation.getOperation() + amount.getValue() + ".00" + "          |     " + balance + ".00";
	}
	
	
	
}
