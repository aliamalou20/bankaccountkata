package com.bank.account.kata;


public class Account {
	
	private Operations operations;
	
	
	public Account(Operations operations) {
		this.operations = operations;
	}
	
	public void deposit(Amount amount) {
		operations.register(amount,Operation.DEPOSIT_OPERATION);
	}
	
	public void withdraw(Amount amount) {
		operations.register(amount,Operation.WITHDRAWAL_OPERATION);
	}
		
	
	public void printStatement(){
		Statement statement = operations.generateStatement();
		statement.printStatement();
	}
	
}
