package com.bank.account.kata;

public enum Operation {
	
    DEPOSIT_OPERATION("+"), 
    WITHDRAWAL_OPERATION("-");

    private final String operation;

    private Operation(String operation) {
		this.operation = operation;
	}

	public String getOperation() {
        return operation;
}
}
