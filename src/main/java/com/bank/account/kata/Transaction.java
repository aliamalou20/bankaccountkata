package com.bank.account.kata;

import java.time.LocalDate;

public class Transaction {
	
	private LocalDate date;
	private Amount amount;
	private Operation operation;
	
	
	public Transaction(LocalDate date, Amount amount, Operation operation) {
		this.date = date;
		this.amount = amount;
		this.operation = operation;
	}
	
	public StatementLine createStatementLine(int accumulatedBalance) {
		int newBalance;
		if(operation.getOperation().equals("+"))
			newBalance = amount.getValue() + accumulatedBalance;
		else
			newBalance = accumulatedBalance - amount.getValue();
		return new StatementLine(date,amount,newBalance, operation);
	}

	@Override
	public String toString() {
		return "Transaction [date=" + date + ", amount=" + amount + "]";
	}
	
	
	
	

}
