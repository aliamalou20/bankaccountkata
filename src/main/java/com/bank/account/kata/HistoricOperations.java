package com.bank.account.kata;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HistoricOperations implements Operations {

	private List<Transaction> transactions = new ArrayList<Transaction>();
	private LocalDate date;
	
	
	public HistoricOperations(LocalDate date) {
		this.date = date;
	}

	@Override
	public void register(Amount amount, Operation operation) {
		transactions.add(new Transaction(date, amount,operation));
	}

	@Override
	public Statement generateStatement() {
		int accumulatedBalance = 0;
		List<StatementLine> statementLines = new ArrayList<>();
		for(Transaction transaction : transactions) {
			StatementLine statementLine = transaction.createStatementLine(accumulatedBalance);
			statementLines.add(statementLine);
			accumulatedBalance = statementLine.getBalance();
		}
		return new Statement(statementLines);
	}

}
