package com.bank.account.kata;

import java.util.Collections;
import java.util.List;

public class Statement {
	
	public static final String HEADER = "   Date      |         Amount        |     Balance";
	
	private List<StatementLine> statementLines;
	
	
	public Statement(List<StatementLine> statementLines) {
		this.statementLines = statementLines;
	}
	

	public void printStatement() {
		System.out.println("--------------------------------------------------");
		System.out.println(HEADER);
		System.out.println("--------------------------------------------------");
		Collections.reverse(statementLines);
		statementLines.forEach( (statementLine) -> System.out.println(statementLine.printStatementLine()));
		System.out.println("--------------------------------------------------");
	}



	

}
